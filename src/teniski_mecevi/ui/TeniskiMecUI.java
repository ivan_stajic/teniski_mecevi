package teniski_mecevi.ui;

import java.util.ArrayList;
import java.util.List;

import teniski_mecevi.dao.TeniserDAO;
import teniski_mecevi.dao.TeniskiMecDAO;
import teniski_mecevi.model.Teniser;
import teniski_mecevi.model.TeniskiMec;
import teniski_mecevi.utils.PomocnaKlasa;

public class TeniskiMecUI {
	
	public static void ispisiMeceveZaTenisera() {
		
		Teniser rez = null;
		List<TeniskiMec> mecevi = new ArrayList<TeniskiMec>();
		int trazeniId = 0;
		
		TeniserUI.ispisiSveTenisere();
		while (rez == null) {
			System.out.println("Unesite id tenisera: ");
			trazeniId = PomocnaKlasa.ocitajCeoBroj();
			rez = TeniserDAO.getTeniserById(ApplicationUI.getConn(), trazeniId);
		}
		
		List<TeniskiMec> listaSvih = TeniskiMecDAO.getAll(ApplicationUI.getConn());
	
		System.out.println("\n=================================================================================");
		System.out.printf("%-4s %-25s %-25s %-15s %-15s", "ID", "IGRAC 1", "IGRAC 2", "POBEDNIK", "PORAZENI");
		System.out.println("\n=================================================================================");
		
		for (int i = 0; i < listaSvih.size(); i++) {
			if((rez.getId() == listaSvih.get(i).getPrviIgrac().getId()) || (rez.getId() == listaSvih.get(i).getDrugiIgrac().getId())){
				mecevi.add(listaSvih.get(i));
			}
		}
		
		for (int i = 0; i < mecevi.size(); i++) {
			System.out.printf("%-4s %-25s %-25s %-15s %-15s\n", mecevi.get(i).getId(),  mecevi.get(i).getPrviIgrac().getIme() + " " + mecevi.get(i).getPrviIgrac().getPrezime(), 
					mecevi.get(i).getDrugiIgrac().getIme() + " " + mecevi.get(i).getDrugiIgrac().getPrezime(), 
					mecevi.get(i).getPobednik(), mecevi.get(i).getPorazeni());

		}
		
		System.out.println("=================================================================================\n");
	}
	
	public static void unesiNoviMec() {
		System.out.println("PRIKAZ SVIH TENISERA:");
		TeniserUI.ispisiSveTenisere();
		
		System.out.print("Unesi id prvog igraca:");
		int prvi = PomocnaKlasa.ocitajCeoBroj();
		while (TeniserUI.pronadjiTeniseraPoId(prvi) == null) {
			System.out.println("Igrac sa id-jem " + prvi
					+ " ne postoji");
			prvi = PomocnaKlasa.ocitajCeoBroj();
		}
		
		System.out.print("Unesi id drugog igraca:");
		int drugi = PomocnaKlasa.ocitajCeoBroj();
		while (TeniserUI.pronadjiTeniseraPoId(drugi) == null) {
			System.out.println("Igrac sa id-jem " + drugi
					+ " ne postoji");
			drugi = PomocnaKlasa.ocitajCeoBroj();
		}

		System.out.print("Unesi prezime pobednika:");
		String pobednik = PomocnaKlasa.ocitajTekst();
		while (TeniserUI.pronadjiTeniseraPoPrezimenu(pobednik) == null) {
			System.out.println("Igrac sa prezimenom " + pobednik
					+ " ne postoji");
			pobednik = PomocnaKlasa.ocitajTekst();
		}
		
		System.out.print("Unesi prezime porazenog:");
		String porazeni = PomocnaKlasa.ocitajTekst();
		while (TeniserUI.pronadjiTeniseraPoPrezimenu(porazeni) == null) {
			System.out.println("Igrac sa prezimenom " + porazeni
					+ " ne postoji");
			porazeni = PomocnaKlasa.ocitajTekst();
		}
		
		TeniskiMec tm = new TeniskiMec(0, TeniserUI.pronadjiTeniseraPoId(prvi),
				TeniserUI.pronadjiTeniseraPoId(drugi), pobednik, porazeni);
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		TeniskiMecDAO.add(ApplicationUI.getConn(), tm);
	}
	
	
	
	

}
