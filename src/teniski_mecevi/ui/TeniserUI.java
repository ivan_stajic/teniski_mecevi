package teniski_mecevi.ui;

import java.util.List;

import teniski_mecevi.dao.TeniserDAO;
import teniski_mecevi.model.Teniser;

public class TeniserUI {
	
	public static void ispisiSveTenisere() {
		System.out.println("\n====================================================");
		System.out.printf("%-4s %-15s %-15s %7s %7s", "ID", "IME", "PREZIME", "G.RODJ.", "G.POC.");
		System.out.println("\n====================================================");

		List<Teniser> teniseri = TeniserDAO.getAll(ApplicationUI.getConn());
		for (Teniser itTeniser: teniseri) {
			System.out.printf("%-4s %-15s %-15s %7s %7s\n", itTeniser.getId(),  itTeniser.getIme(), itTeniser.getPrezime(), 
					itTeniser.getGodinaRodjenja(), itTeniser.getGodinaPocetka());
		}

		System.out.println("====================================================\n");
	}
	
	public static Teniser pronadjiTeniseraPoId(int id) {
		Teniser retVal = TeniserDAO.getTeniserById(ApplicationUI.getConn(),id);
		return retVal;
	}

	public static Teniser pronadjiTeniseraPoPrezimenu(String prezime) {
		Teniser retVal = TeniserDAO.getPlayerByLastName(ApplicationUI.getConn(),
				prezime);
		return retVal;
	}




}
