package teniski_mecevi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import teniski_mecevi.model.Teniser;


public class TeniserDAO {
	
	public static List<Teniser> getAll(Connection conn) {
		List<Teniser> teniseri = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, ime, prezime, godinaRodjenja, godinaPocetka FROM teniseri";

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				int godinaRodjenja = rset.getInt(index++);
				int godinaPocetka = rset.getInt(index++);

				Teniser teniser = new Teniser(id, ime, prezime, godinaRodjenja, godinaPocetka);
				teniseri.add(teniser);
				
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return teniseri;
	}
	
	public static Teniser getTeniserById(Connection conn, int id) {
		Teniser teniser = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT * FROM teniseri WHERE id = " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int idT = rset.getInt(index++);
				String ime = rset.getString(index++);
				String prezime = rset.getString(index++);
				int godinaRodjenja = rset.getInt(index++);
				int godinaPocetka = rset.getInt(index++);
				
				teniser = new Teniser(idT, ime, prezime, godinaRodjenja, godinaPocetka);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return teniser;
	}
	
	public static Teniser getPlayerByLastName(Connection conn, String prezime) {
		Teniser teniser = null;

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM teniseri WHERE prezime = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, prezime);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				String ime = rset.getString(index++);
				String prezimeS = rset.getString(index++);
				int godinaRodjenja = rset.getInt(index++);
				int godinaPocetka = rset.getInt(index++);
				
				teniser = new Teniser(id, ime, prezimeS, godinaRodjenja, godinaPocetka);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return teniser;
	}




}
