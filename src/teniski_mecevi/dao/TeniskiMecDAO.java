package teniski_mecevi.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import teniski_mecevi.model.TeniskiMec;


public class TeniskiMecDAO {
	
	public static TeniskiMec getByPlayerId(Connection conn, int id) {
		TeniskiMec teniskiMec = null;

		Statement stmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, prviIgrac, drugiIgrac, pobednik, porazeni FROM mecevi WHERE mecevi.prviIgrac = " + id + " || mecevi.drugiIgrac = " + id;

			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			if (rset.next()) {
				int index = 1;
				int idM = rset.getInt(index++);
				int prviIgrac = rset.getInt(index++);
				int drugiIgrac = rset.getInt(index++);
				String pobednik = rset.getString(index++);
				String porazeni = rset.getString(index++);
				
				teniskiMec = new TeniskiMec(idM, TeniserDAO.getTeniserById(conn, prviIgrac),
						TeniserDAO.getTeniserById(conn, drugiIgrac), pobednik, porazeni);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return teniskiMec;
	}
	
	public static List<TeniskiMec> getAll(Connection conn) {
		ArrayList<TeniskiMec> mecevi = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM mecevi";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int prviIgrac = rset.getInt(index++);
				int drugiIgrac = rset.getInt(index++);
				String pobednik = rset.getString(index++);
				String porazeni = rset.getString(index++);
				
				TeniskiMec teniskiMec = new TeniskiMec(id, TeniserDAO.getTeniserById(conn, prviIgrac),
						TeniserDAO.getTeniserById(conn, drugiIgrac), pobednik, porazeni);
				mecevi.add(teniskiMec);
				
			}

			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mecevi;
	}

	public static boolean add(Connection conn, TeniskiMec teniskiMec) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO mecevi (prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (?, ?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, teniskiMec.getPrviIgrac().getId());
			pstmt.setInt(index++, teniskiMec.getDrugiIgrac().getId());
			pstmt.setString(index++, teniskiMec.getPobednik());
			pstmt.setString(index++, teniskiMec.getPorazeni());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}



}
