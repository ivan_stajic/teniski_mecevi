package teniski_mecevi.model;

public class TeniskiMec {
	
	protected final int id;
	protected Teniser prviIgrac;
	protected Teniser drugiIgrac;
	protected String pobednik;
	protected String porazeni;
	
	public TeniskiMec(int id, Teniser prviIgrac, Teniser drugiIgrac, String pobednik, String porazeni) {
		super();
		this.id = id;
		this.prviIgrac = prviIgrac;
		this.drugiIgrac = drugiIgrac;
		this.pobednik = pobednik;
		this.porazeni = porazeni;
	}

	@Override
	public String toString() {
		return "TeniskiMec [id=" + id + ", prviIgrac=" + prviIgrac + ", drugiIgrac=" + drugiIgrac + ", pobednik="
				+ pobednik + ", porazeni=" + porazeni + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeniskiMec other = (TeniskiMec) obj;
		if (drugiIgrac == null) {
			if (other.drugiIgrac != null)
				return false;
		} else if (!drugiIgrac.equals(other.drugiIgrac))
			return false;
		if (id != other.id)
			return false;
		if (pobednik == null) {
			if (other.pobednik != null)
				return false;
		} else if (!pobednik.equals(other.pobednik))
			return false;
		if (porazeni == null) {
			if (other.porazeni != null)
				return false;
		} else if (!porazeni.equals(other.porazeni))
			return false;
		if (prviIgrac == null) {
			if (other.prviIgrac != null)
				return false;
		} else if (!prviIgrac.equals(other.prviIgrac))
			return false;
		return true;
	}

	public Teniser getPrviIgrac() {
		return prviIgrac;
	}

	public void setPrviIgrac(Teniser prviIgrac) {
		this.prviIgrac = prviIgrac;
	}

	public Teniser getDrugiIgrac() {
		return drugiIgrac;
	}

	public void setDrugiIgrac(Teniser drugiIgrac) {
		this.drugiIgrac = drugiIgrac;
	}

	public String getPobednik() {
		return pobednik;
	}

	public void setPobednik(String pobednik) {
		this.pobednik = pobednik;
	}

	public String getPorazeni() {
		return porazeni;
	}

	public void setPorazeni(String porazeni) {
		this.porazeni = porazeni;
	}

	public int getId() {
		return id;
	}
	
	
	
	
	

}
