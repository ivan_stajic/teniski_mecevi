package teniski_mecevi.model;

public class Teniser {
	
	protected final int id;
	protected String ime;
	protected String prezime;
	protected int godinaRodjenja;
	protected int godinaPocetka;
	
	public Teniser(int id, String ime, String prezime, int godinaRodjenja, int godinaPocetka) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.godinaRodjenja = godinaRodjenja;
		this.godinaPocetka = godinaPocetka;
	}

	@Override
	public String toString() {
		return "Teniser [id=" + id + ", ime=" + ime + ", prezime=" + prezime + ", godinaRodjenja=" + godinaRodjenja
				+ ", godinaPocetka=" + godinaPocetka + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Teniser other = (Teniser) obj;
		if (godinaPocetka != other.godinaPocetka)
			return false;
		if (godinaRodjenja != other.godinaRodjenja)
			return false;
		if (id != other.id)
			return false;
		if (ime == null) {
			if (other.ime != null)
				return false;
		} else if (!ime.equals(other.ime))
			return false;
		if (prezime == null) {
			if (other.prezime != null)
				return false;
		} else if (!prezime.equals(other.prezime))
			return false;
		return true;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public int getGodinaRodjenja() {
		return godinaRodjenja;
	}

	public void setGodinaRodjenja(int godinaRodjenja) {
		this.godinaRodjenja = godinaRodjenja;
	}

	public int getGodinaPocetka() {
		return godinaPocetka;
	}

	public void setGodinaPocetka(int godinaPocetka) {
		this.godinaPocetka = godinaPocetka;
	}

	public int getId() {
		return id;
	}
	

}
