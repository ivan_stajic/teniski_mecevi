DROP SCHEMA IF EXISTS teniskimecevi;
CREATE SCHEMA teniskimecevi DEFAULT CHARACTER SET utf8;
USE teniskimecevi;

CREATE TABLE teniseri (
	id INT AUTO_INCREMENT, 
	ime VARCHAR(20) NOT NULL, 
    prezime VARCHAR(20) NOT NULL, 
    godinaRodjenja INT NOT NULL,
    godinaPocetka INT NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE mecevi (
	id INT AUTO_INCREMENT, 
	prviIgrac INT NOT NULL, 
	drugiIgrac INT NOT NULL, 
    pobednik VARCHAR(20) NOT NULL,
    porazeni VARCHAR(20) NOT NULL,

	PRIMARY KEY(id), 
	 
	FOREIGN KEY (prviIgrac) REFERENCES teniseri(id)
		ON DELETE RESTRICT, 
	FOREIGN KEY (drugiIgrac) REFERENCES teniseri(id)
		ON DELETE RESTRICT
);
	
INSERT INTO teniseri (id, ime, prezime, godinaRodjenja, godinaPocetka) VALUES (1, 'Novak', 'Djokovic', 1987, 2003);
INSERT INTO teniseri (id, ime, prezime, godinaRodjenja, godinaPocetka) VALUES (2, 'Roger', 'Federer', 1981, 1998);
INSERT INTO teniseri (id, ime, prezime, godinaRodjenja, godinaPocetka) VALUES (3, 'Rafael', 'Nadal', 1986, 2001);
INSERT INTO teniseri (id, ime, prezime, godinaRodjenja, godinaPocetka) VALUES (4, 'Stan', 'Wawrinka', 1985, 2002);
INSERT INTO teniseri (id, ime, prezime, godinaRodjenja, godinaPocetka) VALUES (5, 'Marin', 'Cilic', 1988, 2005);
INSERT INTO teniseri (id, ime, prezime, godinaRodjenja, godinaPocetka) VALUES (6, 'Alexander', 'Zverev', 1997, 2013);

INSERT INTO mecevi (id, prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (1, 1, 3, 'Djokovic', 'Nadal');
INSERT INTO mecevi (id, prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (2, 2, 4, 'Federer', 'Wawrinka');
INSERT INTO mecevi (id, prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (3, 3, 5, 'Nadal', 'Cilic');
INSERT INTO mecevi (id, prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (4, 4, 6, 'Wawrinka', 'Zverev');
INSERT INTO mecevi (id, prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (5, 1, 4, 'Djokovic', 'Wawrinka');
INSERT INTO mecevi (id, prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (6, 2, 6, 'Federer', 'Zverev');
INSERT INTO mecevi (id, prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (7, 3, 1, 'Nadal', 'Djokovic');
INSERT INTO mecevi (id, prviIgrac, drugiIgrac, pobednik, porazeni) VALUES (8, 4, 2, 'Wawrinka', 'Federer');

